package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class ExcelData {
		   @DataProvider
		    public String[][] getdata() throws IOException{ 
		        File f=new File("/home/nagaraj/Documents/Testdata2 login.xlsx");
		        FileInputStream fis=new FileInputStream(f);
		        XSSFWorkbook b=new XSSFWorkbook(fis);
		        XSSFSheet sheet=b.getSheetAt(0);
		        int rows=sheet.getPhysicalNumberOfRows();
		        int columns=sheet.getRow(0).getLastCellNum();
		        
		        
		        String[][] data=new String[rows-1][columns];
		        for(int i=0;i<rows-1;i++) {
		            for(int j=0;j<columns;j++) {
		                DataFormatter data1=new DataFormatter();
		                String celldata=sheet.getRow(i).getCell(j).getStringCellValue();
		                data[i][j]=data1.formatCellValue(sheet.getRow(i+1).getCell(j));
		                
		            }
		        }
		        return data;
		    }



	}


