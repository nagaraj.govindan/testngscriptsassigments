package com.data;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class DataProvider {

		public class NewTest {
		    @Test(dataProvider = "getdata",dataProviderClass=ExcelData.class)
		    public void loginApplication(String username, String password) throws InterruptedException {
		        WebDriver driver = new ChromeDriver();
		        driver.manage().window().maximize();
		        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

		        driver.get("https://demowebshop.tricentis.com/login");
		        driver.findElement(By.id("Email")).sendKeys(username);
		        Thread.sleep(4000);
		        driver.findElement(By.id("Password")).sendKeys(password);
		        driver.findElement(By.xpath("//input[@value='Log in']")).click();
		        driver.findElement(By.linkText("Log out")).click();
		       
		        
		  	  Assert.assertTrue(driver.findElement(By.id("welcome")).isDisplayed());
		  	  Thread.sleep(3000);
                
                
		    }


		}
		      
		
	}

