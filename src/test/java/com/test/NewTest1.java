package com.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class NewTest1 {
	@Test(dependsOnMethods = "newCustomer" )
	public void modifyCustomer()
	{
		System.out.println("The customer will get modified");
	}
	
	@Test(dependsOnMethods ="modifyCustomer" )
	public void createCustomer()
	{
		System.out.println("The customer will get created");
	}
	
	@Test
	public void newCustomer()
	{
		System.out.println("The customer will get newcustomer");
	}
	@BeforeClass
	public void BeforeClass()
	{
		System.out.println("start database connection , launch browser ");
	}
	
	@AfterClass
	public void AfterClass()
	{
		System.out.println("close database connection , close browser ");
	}
	
}

