package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingMultipleData {

	public static void main(String[] args) throws IOException {
		
		WebDriver driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("https://demowebshop.tricentis.com/login");
		 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		 
		 File f=new File("/home/nagaraj/Documents/Testdata2 login.xlsx");
			FileInputStream fis= new FileInputStream(f);
			XSSFWorkbook book = new XSSFWorkbook(fis);
			XSSFSheet sheet= book.getSheetAt(0);
			int rows=sheet.getPhysicalNumberOfRows();
		
			for(int i=1; i<rows; i++) {
		   String username=sheet.getRow(i).getCell(0).getStringCellValue();
		   String password=sheet.getRow(i).getCell(1).getStringCellValue();
		 
		   
			 driver.findElement(By.id("Email")).sendKeys(username);
			 driver.findElement(By.id("Password")).sendKeys(password);
			 driver.findElement(By.xpath("//input[@value='Log in']")).click();
			 driver.findElement(By.linkText("Log out")).click();
		  //   String text=driver.findElement(By.linkText("n95000030320@gmail.com")).getText(); 
			// System.out.println(text);
		 
		 
		 
		 
			} 
	}

}
