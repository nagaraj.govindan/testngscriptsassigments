package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class LoginClass {

	public static void main(String[] args) throws BiffException, IOException {
		// TODO Auto-generated method stub
		 WebDriver driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("https://demowebshop.tricentis.com/login");
		 

         File f = new File("/home/nagaraj/Documents/Testdata.xls");
		FileInputStream fi=new FileInputStream(f);
		Workbook book= Workbook.getWorkbook(fi);
		Sheet st = book.getSheet("Sheet1");

		int rows=st.getRows();
		int columns = st.getColumns();
		
		for(int i=1; i<rows; i++) {
		
		String	username=st.getCell(0,i).getContents();
		String	password=st.getCell(1,i).getContents();
		 
		 driver.findElement(By.id("Email")).sendKeys(username);
		 driver.findElement(By.id("Password")).sendKeys(password);
		 driver.findElement(By.xpath("//input[@value='Log in']")).click();
	     String text=driver.findElement(By.linkText("n95000030320@gmail.com")).getText(); 
		 System.out.println(text);
	}

}
}
