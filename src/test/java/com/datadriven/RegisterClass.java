package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterClass {

	public static void main(String[] args) throws BiffException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("https://demowebshop.tricentis.com/register");
		   File f = new File("/home/nagaraj/Documents/Testdata.xls");
			FileInputStream fi=new FileInputStream(f);
			Workbook book= Workbook.getWorkbook(fi);
			Sheet st = book.getSheet("Registerdata");

			int rows=st.getRows();
			int columns = st.getColumns();
			
			for(int i=1; i<rows; i++) {
			
			String	firstname=st.getCell(0,i).getContents();
			String	lastname=st.getCell(1,i).getContents();
			String	email=st.getCell(2,i).getContents();
			String	password=st.getCell(3,i).getContents();
			String	confirmpassword=st.getCell(4,i).getContents();
			
		   driver.findElement(By.xpath("//a[@class='ico-register']")).click();

	        driver.findElement(By.xpath("//input[@id='gender-male']")).click();
	        Thread.sleep(1000);
	        driver.findElement(By.xpath("//input[@id='FirstName']")).sendKeys(firstname);
			
	        driver.findElement(By.xpath("//input[@id='LastName']")).sendKeys(lastname);

	        driver.findElement(By.xpath("//input[@name='Email']")).sendKeys(email);

	        driver.findElement(By.xpath("//input[@name='Password']")).sendKeys(password);

	        driver.findElement(By.xpath("//input[@name='ConfirmPassword']")).sendKeys(confirmpassword);
	          
	         Thread.sleep(1000);
	         driver.findElement(By.xpath("//input[@name='register-button']")).click();
	         String text1=driver.findElement(By.linkText("n95000030320@gmail.com")).getText(); 
			 System.out.println(text1);

	         driver.quit();

			 
		}
	}

}
