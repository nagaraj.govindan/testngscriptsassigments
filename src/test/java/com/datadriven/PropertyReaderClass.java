package com.datadriven;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyReaderClass {
	static Properties prop = new Properties();

	public static void initProperty() {
		try {
			prop.load(new FileInputStream("/home/nagaraj/Documents/TestNgscripts/src/test/resources/config/userlogin.properties"));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		return prop.getProperty(key);
	}
}

